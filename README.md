# PGLW - Parametric Geometry Library for Wind
[![pipeline status](https://gitlab.windenergy.dtu.dk/frza/PGL/badges/master/pipeline.svg)](https://gitlab.windenergy.dtu.dk/frza/PGL/commits/master)
[![coverage report](https://gitlab.windenergy.dtu.dk/frza/PGL/badges/master/coverage.svg)](https://gitlab.windenergy.dtu.dk/frza/PGL/commits/master)

PGLW is a Python based tool for creating surface geometries using simple parametric inputs developed
at the Department of Wind and Energy Systems of the Technical University of Denmark.
PGLW is tailored towards wind turbine related geometries, but its base classes can be used for any purpose.
The package contains a series of classes for generating geometric primitives such as airfoils, blade surfaces, nacelles, towers etc,
that can be run as scripts with input files.

## Installation and requirements

PGLW installs as a standard Python distribution and requires Python >=3.8.

To install PGLW in developer mode simply run

    $ pip install -e .[test,docs]

Or install the latest tagged release wheel:

    pip install PGLW --index-url https://__token__:<your_personal_token>@gitlab.windenergy.dtu.dk/api/v4/projects/66/packages/pypi/simple

## Documentation

Documentation is available here: https://frza.pages.windenergy.dtu.dk/PGL.

PGLW is documented using Sphinx, and you can build the docs locally by navigating to the ``docs`` directory and issueing the command:

    $ make html

To view the docs open _build/html/index.html in your a browser.

## Examples

A number of examples of how to use PGLW is located in ``PGLW/examples``.
