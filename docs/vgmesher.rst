
Vortex Generator Mesher
=======================

The :class:`PGL.components.vgmesher.VGMesher` class generates a 3D surface mesh on an extruded airfoil section fitted with a delta-shaped vortex generator.
You can specify the VG's chordwise location, length, height, angle, intra- and inter-spacing, which is illustrated in the figure below.

.. vgmesher-schematic-fig:

.. figure:: /figures/vg_schematic.*
    :align: center

While the figure shows several VG pairs, we normally only simulate a single VG vane, indicated in dark grey, and apply symmetry boundary conditions on the lateral boundaries, which is equivalent to an infinite array of VGs.

In :file:`PGL.examples.vg_example.py` you can find an example that generates a VG on an FFA-W3-301 airfoil:

.. literalinclude:: ../PGL/examples/vg_example.py

In addition to the parameters described above, you also need to specify the number of points on the surface of the airfoil in the chordwise direction, as well as the number of points on the blunt trailing edge.
The remaining parameters control the placement of connectors on the VG unit.

The ``VGMesher`` class calls the :class:`PGL.components.vgunit.VGUnit` class which generates a VG unit with a normalized height of 1, and the VG leading edge at (0, 0, 0).
The ``VGMesher`` class takes care of scaling, placing and projecting the VG unit onto the airfoil at the specified chordwise position ``xc_vg``.

.. vgunit-fig:

.. figure:: /figures/vgunit.*
    :align: center

.. vgairfoil-fig:

.. figure:: /figures/vgairfoil.*
    :align: center
